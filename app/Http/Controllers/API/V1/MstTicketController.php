<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\MstTicket;
use App\Models\Stop;
use Dotenv\Exception\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class MstTicketController extends Controller
{
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'place' => 'required|unique:stops|max:255',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }
            $places = Stop::get();

            $place = new Stop();
            $place->place = $request->place;
            $place->save();

            if (count($places) > 0) {
                foreach ($places as $placeRow) {

                    $combinations[] = [
                        "place_from" => $placeRow->place,
                        "place_to" => $request->place,
                        "created_at" => Carbon::now(),
                        "updated_at" => Carbon::now(),
                    ];

                    $combinations[] = [
                        "place_from" => $request->place,
                        "place_to" => $placeRow->place,
                        "created_at" => Carbon::now(),
                        "updated_at" => Carbon::now(),
                    ];
                }
                
                MstTicket::insert($combinations);
            }

            return response()->json(['status' => true, 'message' => 'Store place successfully', 'data' => $place]);
        } catch (\Throwable $th) {
            return response()->json(['status' => false, 'message' => $th->getMessage()]);
        }
    }

    public function mstTickets(Request $request)
    {
        try {

            if ($request->has('status')) {
                $mstTickets = MstTicket::Where('status', $request->status)->get();
            } else {
                $mstTickets = MstTicket::all();
            }
            if (count($mstTickets) > 0) {
                return response()->json(['status' => true, "message" => "Data Found.", "data" => $mstTickets]);
            }

            return response()->json(['status' => false, "message" => "Data Not Found."]);
        } catch (\Throwable $th) {
            return response()->json(['status' => false, "message" => $th->getMessage()]);
        }
    }

    public function updateMstTickets(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'price' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }

            $mstTicket = MstTicket::find($request->id);

            if ($mstTicket) {
                $mstTicket->price = $request->price;
                $mstTicket->status = 'active';
                $mstTicket->save();

                $combinationTicket = MstTicket::where('place_from', $mstTicket->place_to)->where('place_to', $mstTicket->place_from)->first();

                if ($combinationTicket) {
                    $combinationTicket->price = $request->price;
                    $combinationTicket->status = 'active';
                    $combinationTicket->save();
                }

                return response()->json(['status' => true, "message" => "Data Found.", "data" => $mstTicket]);
            }
        } catch (\Throwable $th) {
            return response()->json(['status' => false, "message" => $th->getMessage()]);
        }
    }
}
