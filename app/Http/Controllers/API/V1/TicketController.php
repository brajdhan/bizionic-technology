<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\MstTicket;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class TicketController extends Controller
{
    public function bookTicket(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'from' => 'required',
                'to' => 'required',
                'members' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }

            $ticket = MstTicket::where('place_from', $request->from)->where('place_to', $request->to)->where('status', 'active')->first();

            if ($ticket) {
                if ($ticket->price != null && $ticket->price > 0) {
                    $totalPrice = $ticket->price * $request->members;

                    $bookTicket = new Ticket();
                    $bookTicket->place_from = $request->from;
                    $bookTicket->place_to = $request->to;
                    $bookTicket->price = $ticket->price;
                    $bookTicket->members = $request->members;
                    $bookTicket->total_price = $totalPrice;
                    $bookTicket->save();

                    return response()->json(['status' => true, 'message' => "Ticket Booked", 'data' => $bookTicket]);
                } else {
                    return response()->json(['status' => false, 'message' => "Price Must be required"]);
                }
            }
        } catch (\Throwable $th) {
            return response()->json(['status' => false, "message" => $th->getMessage()]);
        }
    }

    public function bookTicketsRecords(){
        try {
             $ticket = Ticket::orderBy('id','DESC')->get();
             if(count($ticket) >0)
             {
                return response()->json(['status' => true, 'message' => "Data Found", 'data' => $ticket]);
            } else {
                return response()->json(['status' => false, 'message' => "Data Not Found"]);
            }
        } catch (\Throwable $th) {
            return response()->json(['status' => false, "message" => $th->getMessage()]);
        }
    }


    public function ticketsHistory($id){
        try {
             $ticket = Ticket::find($id);
             if($ticket)
             {
                return response()->json(['status' => true, 'message' => "Data Found", 'data' => $ticket]);
            } else {
                return response()->json(['status' => false, 'message' => "Data Not Found"]);
            }
        } catch (\Throwable $th) {
            return response()->json(['status' => false, "message" => $th->getMessage()]);
        }
    }
}
