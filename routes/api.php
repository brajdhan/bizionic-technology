<?php

use App\Http\Controllers\API\V1\MstTicketController;
use App\Http\Controllers\API\V1\TicketController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('add-place',[MstTicketController::class,'store']);
Route::get('mst-tickets',[MstTicketController::class,'mstTickets']);
Route::post('update-mst-tickets',[MstTicketController::class,'updateMstTickets']);
Route::post('book-ticket',[TicketController::class,'bookTicket']);
Route::get('book-ticket-records',[TicketController::class,'bookTicketsRecords']);
Route::get('ticket-history/{id}',[TicketController::class,'ticketsHistory']);

